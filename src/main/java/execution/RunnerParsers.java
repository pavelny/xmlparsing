package execution;

import domain.Homes;
import execution.SAXParser.SAXHandler;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.nio.charset.StandardCharsets;

import static java.lang.System.out;

public class RunnerParsers {

    private final static String sourceXml = "src/main/resources/Homes.xml";
    private final static String xsdSchema = "src/main/resources/definition.xsd";
    private final static String toMarshall = "src/main/resources/Marshall.xml";
    private final static File sourceXmlFile = new File(sourceXml);

    //TODO исключения надо обрабатывать внутри методов валидации и парсинга? или в main в момент вызова?
    public static void main(String[] args)throws Exception {

        //валидация способом, который я сделал на курсах в epam'e год назад
        Validation xml = new Validation();
        xml.validate(sourceXml, xsdSchema);

        //validation from oracle example
        OracleValidation validation = new OracleValidation();
        validation.validate(sourceXml, xsdSchema);

        //DOM parser
        DomParserDemo parserDemo = new DomParserDemo();
        Homes homeDom = parserDemo.parse(sourceXml);
        out.println("DOM parser " + homeDom);

        //SAX parser
        SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
        SAXHandler handler = new SAXHandler();
        saxParser.parse(sourceXmlFile, handler);
        Homes homeSax = handler.getHomes();
        out.println("SAX parser " + homeSax);

        //StAX parser with XMLStreamReader
        StAXParserDemo demo = new StAXParserDemo();
        Homes homesStax = demo.parse(sourceXml);
        out.println("StAX parser uses XMLStreamReader" + homesStax);

        //StAX parser with XMLEventReader
        StAXEvents events = new StAXEvents();
        Homes kraken = events.parse(sourceXml);
        out.println("StAX parser uses XMLEventReader" + kraken);

        Marshaller marshaller = JAXBContext.newInstance(Homes.class).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());
        marshaller.marshal(homesStax, new File(toMarshall));
        //marshaller.marshal(homesStax, System.out);

        Unmarshaller unmarshaller = JAXBContext.newInstance(Homes.class).createUnmarshaller();
        Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                .newSchema(new File(xsdSchema));
        unmarshaller.setSchema(schema);
        Homes home2 = (Homes) unmarshaller.unmarshal(sourceXmlFile);
        out.print("From unmarshaller: " + home2);
    }
}
