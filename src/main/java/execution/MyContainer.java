package execution;

import enterprise.Parseble;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

public class MyContainer {

    public <T> T execute(Parseble<T> parser, String inputXml){
        Class<? extends Parseble> impl = parser.getClass();
        if(impl.isAnnotationPresent(Friendly.class)) {
            System.out.println("Hello, we've annotations:");
            List<Annotation> annotation = Arrays.asList(impl.getAnnotations());
            annotation.forEach(annos -> {
                String[] s = String.valueOf(annos)
                        .split("\\.");
                if(s.length != 0) {
                    System.out.println(s[s.length - 1]);
                }
            });
        } else {
            System.out.printf("Strict mode for %s", impl);
        }
        return parser.parse(inputXml);
    }
}
