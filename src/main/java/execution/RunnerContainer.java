package execution;

public class RunnerContainer {

    private final static String sourceXml = "src/main/resources/Homes.xml";

    public static void main(String[] args) {
        MyContainer container = new MyContainer();
        container.execute(new DomParserDemo(), sourceXml);
        container.execute(new StAXEvents(), sourceXml);
    }
}
