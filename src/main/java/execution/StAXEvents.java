/**
 * Здесь используется StAX парсер, применяя  <code>XMLEventRreader</code>, а в классе
 * StAXParserDemo используется <code>XMLStreamRreader</code>
 */
package execution;

import domain.Address;
import domain.Apartment;
import domain.Homes;
import domain.LoftType;
import domain.Room;
import domain.WallType;
import enterprise.Parseble;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

public class StAXEvents implements Parseble<Homes> {

    private Homes homes;
    private Room room;
    private int windows;
    private int doors;
    private WallType wall;
    private Address address;
    private LoftType loft;
    private String element;

    public Homes parse(String inputFile) {

        XMLEventReader eventReader = null;
        try {
            eventReader = XMLInputFactory.newInstance()
                    .createXMLEventReader(new FileReader(inputFile));
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_DOCUMENT:
                        homes = new Homes();
                        break;
                    case XMLStreamConstants.START_ELEMENT:
                        element = event.asStartElement().getName().getLocalPart();
                        if (element.equalsIgnoreCase("address")) {
                            address = new Address();
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        if (characters.isWhiteSpace()) {
                            break;
                        }
                        if ("city".equals(element)) {
                            address.setCity(characters.getData());
                        } else if ("street".equals(element)) {
                            address.setStreet(characters.getData());
                        } else if ("number".equals(element)) {
                            address.setNumber(Integer.parseInt(characters.getData()));
                        } else if ("type".equals(element)) {
                            wall = WallType.valueOf(characters.getData());
                        } else if ("windows".equals(element)) {
                            windows = Integer.parseInt(characters.getData());
                        } else if ("doors".equals(element)) {
                            doors = Integer.parseInt(characters.getData());
                        } else if ("loft".equals(element)) {
                            loft = LoftType.valueOf(characters.getData());
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();
                        if ("rooms".equals(endElement.getName().getLocalPart())) {
                            room = new Room.Builder(wall).doors(doors).windows(windows).build();
                        } else if ("apartments".equals(endElement.getName().getLocalPart())) {
                            homes.addApartment(createApartment());
                        }
                        break;
                }
            }
        } catch (XMLStreamException | IOException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (eventReader != null) {
                    eventReader.close();
                }
            } catch (XMLStreamException e) {
                System.err.println(e.getMessage());
            }
        }
        return homes;
    }

    private Apartment createApartment() {
        Apartment apartment = new Apartment();
        apartment.setAddress(address);
        apartment.setRooms(Collections.singletonList(room));
        apartment.setLoft(loft);
        return apartment;
    }
}