package execution.jaxbExamples;

import domain.Homes;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

public class JaxbElementDemo {

    public static void main(String[] args) throws JAXBException {
        Homes homes = HomesUtils.createHomes();

        JAXBElement<Homes> jaxbElement = new JAXBElement<>(new QName(Homes.class.getSimpleName()), Homes.class, homes);

        Class<Homes> declaredType = jaxbElement.getDeclaredType();
        System.out.println(declaredType.getName());

        QName qName = jaxbElement.getName();
        System.out.println(qName.getLocalPart());

        Class clazz = jaxbElement.getScope();
        System.out.println(clazz.getName());
        System.out.println(clazz.getCanonicalName());
        System.out.println(clazz.getModifiers());

        Homes h = jaxbElement.getValue();
        System.out.println(h);
    }
}
