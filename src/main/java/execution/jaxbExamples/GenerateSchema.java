package execution.jaxbExamples;

import domain.Homes;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;

public class GenerateSchema {

    public static void main(String[] args)throws Exception {
        JAXBContext context = JAXBContext.newInstance(Homes.class);
        SchemaOutputResolver sor = new SchemaResolver();

        context.generateSchema(sor);
    }
}
