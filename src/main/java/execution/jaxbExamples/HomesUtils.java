package execution.jaxbExamples;

import domain.Homes;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class HomesUtils {

    private final static String sourceXml = "src/main/resources/Homes.xml";

    public static Homes createHomes()throws JAXBException {
        Unmarshaller unmarshaller = JAXBContext.newInstance(Homes.class).createUnmarshaller();
        return (Homes) unmarshaller.unmarshal(new File(sourceXml));
    }
}
