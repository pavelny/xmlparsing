package execution.jaxbExamples;

import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class SchemaResolver extends SchemaOutputResolver {

    private final static String output = "src/main/resources/GeneratedSchema.xsd";

    @Override
    public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
        suggestedFileName = output;
        File file = new File(suggestedFileName);
        StreamResult result = new StreamResult(file);
        result.setSystemId(file.toURI().toURL().toString());
        return result;
    }
}
