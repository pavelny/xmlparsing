package execution.jaxbExamples;

import domain.Homes;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.Binder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JaxbBinder {

    private final static String toBind = "src/main/resources/JaxbBinder.xml";

    public static void main(String[] args) throws JAXBException, IOException {
        Homes homes = HomesUtils.createHomes();

        try {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Binder<Node> binder = JAXBContext.newInstance(Homes.class).createBinder(Node.class);
            binder.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);//don't work. Result format don't output
            binder.marshal(homes, document);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            //todo in result file write not formated content, but I set JAXB_FORMATTED_OUTPUT property
            transformer.transform(new DOMSource(document), new StreamResult(new BufferedWriter(new FileWriter(new File(toBind)))));
        } catch (TransformerException | ParserConfigurationException e) {
            e.getMessage();
        }
    }
}
