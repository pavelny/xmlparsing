package execution.SAXParser;

import domain.Address;
import domain.Apartment;
import domain.Homes;
import domain.LoftType;
import domain.Room;
import domain.WallType;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SAXHandler extends DefaultHandler {

    private Homes homes;
    private Room room;
    private int windows;
    private WallType wall;
    private Address address;
    private LoftType loft;
    private String element;
    private List<Apartment> apps;

    @Override
    public void startDocument() throws SAXException {
        homes = new Homes();
        apps = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        element = qName;
        if (element.equalsIgnoreCase("address")) {
            address = new Address();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        element = "";
        if (qName.equalsIgnoreCase("apartments")) {
            apps.add(createApartment());
        }
    }

    @Override
    public void endDocument() throws SAXException {
        homes.setApartments(apps);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (element.equalsIgnoreCase("city")) {
            address.setCity(new String(ch, start, length));
        } else if (element.equalsIgnoreCase("street")) {
            address.setStreet(new String(ch, start, length));
        } else if (element.equalsIgnoreCase("number")) {
            address.setNumber(Integer.parseInt(new String(ch, start, length)));
        } else if (element.equalsIgnoreCase("type")) {
            wall = WallType.valueOf(new String(ch, start, length));
        } else if (element.equalsIgnoreCase("windows")) {
            windows = Integer.parseInt(new String(ch, start, length));
        } else if (element.equalsIgnoreCase("doors")) {
            int doors = Integer.parseInt(new String(ch, start, length));
            room = new Room.Builder(wall).doors(doors).windows(windows).build();
        } else if (element.equalsIgnoreCase("loft")) {
            loft = LoftType.valueOf(new String(ch, start, length));
        }
    }

    private Apartment createApartment() {
        Apartment apartment = new Apartment();
        apartment.setAddress(address);
        apartment.setRooms(Collections.singletonList(room));
        apartment.setLoft(loft);
        return apartment;
    }

    public Homes getHomes() {
        return homes;
    }
}
