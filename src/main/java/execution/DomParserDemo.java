package execution;

import domain.Address;
import domain.Apartment;
import domain.Homes;
import domain.LoftType;
import domain.Room;
import domain.WallType;
import enterprise.DOM;
import enterprise.Parseble;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@DOM
@Friendly("Annotated class")
public class DomParserDemo implements Parseble<Homes> {

    @Friendly
    public Homes parse(String inputXml) {

        Homes homes = new Homes();
        //checkAnnotation();

        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(inputXml);
            if (document != null) {
                Element root = document.getDocumentElement();
                root.normalize();
                NodeList apartments = root.getChildNodes();

                homes.setApartments(parseDocument(apartments));
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.err.println(e.getMessage() + e.getStackTrace());
        }
        return homes;
    }

    private List<Apartment> parseDocument(NodeList apartments) {
        List<Apartment> apps = new ArrayList<>();
        for (int temp = 0; temp < apartments.getLength(); temp++) {
            Node apartmentNode = apartments.item(temp);

            if (apartmentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) apartmentNode;
                Apartment apartment = createApartment(element);
                apps.add(apartment);
            }
        }
        return apps;
    }

    private String getElement(Element element, String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        if (nodeList != null && nodeList.item(0) != null) {
            return nodeList.item(0).getTextContent();
        } else {
            return null;
        }
    }

    private Address createAdress(Element element) {
        Address address = new Address();
        address.setCity(getElement(element, "city"));
        address.setStreet(getElement(element, "street"));
        address.setNumber(Integer.parseInt(getElement(element, "number")));
        return address;
    }

    private WallType createWall(Element element) {
        return WallType.valueOf(getElement(element, "type"));
    }

    private Room createRoom(Element element) {
        WallType wall = createWall(element);
        int doors = Integer.parseInt(getElement(element, "doors"));
        int windows = Integer.parseInt(getElement(element, "windows"));
        return new Room.Builder(wall).doors(doors).windows(windows).build();
    }

    private LoftType createLoft(Element element) {
        return LoftType.valueOf(getElement(element, "loft"));
    }

    private Apartment createApartment(Element element) {
        Apartment apartment = new Apartment();
        apartment.setAddress(createAdress(element));
        apartment.setRooms(Collections.singletonList(createRoom(element)));
        apartment.setLoft(createLoft(element));
        return apartment;
    }

    public void checkAnnotation() {
        final String methodName = "parse";
        Class<? extends DomParserDemo> clazz = this.getClass();
        try {
            Method method = clazz.getMethod(methodName, String.class);
            if (method.isAnnotationPresent(Friendly.class)) {
                Friendly myAnno = method.getAnnotation(Friendly.class);
                System.out.printf("Hello from: %s with annotation: %s\n", method, myAnno.toString());
            }
        } catch (NoSuchMethodException e) {
            System.err.printf("No method %s with %s arguments\n", methodName, "File");
        }
        if (clazz.isAnnotationPresent(Friendly.class)) {
            Friendly myAnno = clazz.getAnnotation(Friendly.class);
            System.out.println(myAnno.value());
        }
    }
}
