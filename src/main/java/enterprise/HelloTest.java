package enterprise;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class HelloTest {

    private final static String sourceXml = "src/main/resources/Homes.xml";
    private final static String xsdSchema = "src/main/resources/definition.xsd";
    @Test
    public void shouldDisplayHelloWorld() {
        WeldContainer weld = new Weld().initialize();
        Service hello = weld.instance().select(Service.class).get();
        assertEquals("should say Hello World !!!", "Hello World !!!", hello.getHomesFromXml("src/main/resources/Homes.xml"));
    }
}
