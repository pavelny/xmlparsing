package domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

@XmlType(propOrder = { "city", "street", "number"})
public class Address {

    private String city;

    private String street;

    private long number;

    public Address() {
    }

    public Address(String city, String street, long number) {
        this.city = city;
        this.street = street;
        this.number = number;
    }

    //todo XmlElement надо использовать для сеттеров или геттеров?
    @XmlElement(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @XmlElement(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @XmlElement(name = "number")
    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return number == address.number &&
                Objects.equals(city, address.city) &&
                Objects.equals(street, address.street);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, street, number);
    }

    @Override
    public String toString() {
        return "Address: " +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number=" + number + ';';
    }
}