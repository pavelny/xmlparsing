package domain;

import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

@XmlType(propOrder = {"type", "windows", "doors"})
public class Room {

    private WallType type;

    private int windows;

    private int doors;

    public Room() {
    }

    private Room(Builder builder) {
        type = builder.type;
        windows = builder.windowsBuilder;
        doors = builder.doorsBuilder;
    }

    public WallType getType() {
        return type;
    }

    public void setType(WallType type) {
        this.type = type;
    }

    public int getWindows() {
        return windows;
    }

    public void setWindows(int windows) {
        this.windows = windows;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return windows == room.windows &&
                doors == room.doors &&
                Objects.equals(type, room.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, windows, doors);
    }

    @Override
    public String toString() {
        return "Room: " +
                "type: " + type +
                ", windows=" + windows +
                ", doors=" + doors;
    }

    public static class Builder {

        private WallType type; //обязателньый параметр

        //optional params
        private int windowsBuilder = 0;

        private int doorsBuilder = 0;

        public Builder(WallType type) {
            this.type = type;
        }

        public Builder windows(int val) {
            windowsBuilder = val;
            return this;
        }

        public Builder doors(int val) {
            doorsBuilder = val;
            return this;
        }

        public Room build() {
            return new Room(this);
        }
    }
}