package domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = { "address", "rooms", "loft"})
public class Apartment {

    private Address address;

    private List<Room> rooms;

    private LoftType loft;

    public Apartment() {
    }

    public Apartment(Address address, List<Room> rooms, LoftType loft) {
        this.address = address;
        this.rooms = rooms;
        this.loft = loft;
    }

    @XmlElement(name = "address")
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @XmlElement(name = "rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @XmlElement(name = "loft")
    public LoftType getLoft() {
        return loft;
    }

    public void setLoft(LoftType loft) {
        this.loft = loft;
    }

    @Override
    public String toString() {
        return "Apartment{" +
                address +
                rooms +
                ", loft: " + loft +
                "}\n";
    }
}